/*
 Navicat Premium Data Transfer

 Source Server         : 本地mysql
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : 192.168.31.32:3306
 Source Schema         : server2019

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 17/07/2021 21:03:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for b站自动签到表
-- ----------------------------
DROP TABLE IF EXISTS `b站自动签到表`;
CREATE TABLE `b站自动签到表`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `DEDEUSERID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '浏览器cookie',
  `SESSDATA` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '浏览器cookie',
  `BILI_JCT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '浏览器cookie',
  `reserveCoins` int NULL DEFAULT 100 COMMENT '预留的硬币数，默认100',
  `upLive` int NULL DEFAULT 0 COMMENT '送礼 up 主的 uid，默认0为自己',
  `chargeForLove` int NULL DEFAULT 0 COMMENT '充电对象的 uid，默认0为自己',
  `userAgent` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 't1' COMMENT '浏览器 UA，默认浏览器UA表的t1',
  `skipDailyTask` int NULL DEFAULT 0 COMMENT '跳过签到，默认0不跳过，其他为跳过',
  `lastTime` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '自动记录的最后执行时间',
  `log` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '正常' COMMENT '最后的日志信息，记录成功或失败',
  `备注` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
