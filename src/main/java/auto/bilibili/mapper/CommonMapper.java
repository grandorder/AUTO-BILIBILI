package auto.bilibili.mapper;

import auto.bilibili.bean.b站签到用户;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CommonMapper {
    List<b站签到用户> selectAll();

    String queryUserAgent(String ua);

    void updateTaskInfo(String userId, String message);
}
