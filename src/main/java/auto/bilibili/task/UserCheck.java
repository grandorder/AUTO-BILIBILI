package auto.bilibili.task;

import auto.bilibili.apiquery.ApiList;
import auto.bilibili.pojo.userinfobean.Data;
import auto.bilibili.utils.HelpUtil;
import auto.bilibili.utils.HttpUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static auto.bilibili.task.TaskInfoHolder.STATUS_CODE_STR;
import static auto.bilibili.task.TaskInfoHolder.userInfo;
/**
 * 登录检查
 *
 * @author @JunzhouLiu @Kurenai
 * @since 2020-11-22 4:57
 */
@Slf4j
public class UserCheck implements Task {

    private final String taskName = "登录检查";

    @Override
    public void run() {
        String requestPram = "";
        JsonObject userJson = HttpUtil.doGet(ApiList.LOGIN + requestPram);
        if (userJson == null) {
            log.info("用户信息请求失败，如果是412错误，请在config.json中更换UA，412问题仅影响用户信息确认，不影响任务");
        } else {
            userJson = HttpUtil.doGet(ApiList.LOGIN);
            //判断Cookies是否有效
            if (userJson.get(STATUS_CODE_STR).getAsInt() == 0
                    && userJson.get("data").getAsJsonObject().get("isLogin").getAsBoolean()) {
                userInfo = new Gson().fromJson(userJson
                        .getAsJsonObject("data"), Data.class);
                log.info("Cookies有效，登录成功");
                log.info("用户名称: {}", HelpUtil.userNameEncode(userInfo.getUname()));
                log.info("硬币余额: " + userInfo.getMoney());
            } else {
                log.debug(String.valueOf(userJson));
                log.warn("Cookies可能失效了,请仔细检查Github Secrets中DEDEUSERID SESSDATA BILI_JCT三项的值是否正确、过期");
                //以下可以删除
                String Url="http://192.168.31.150:10303/weixin/textMessageA/Cookies可能失效了,请检查密钥";
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> entity= restTemplate.getForEntity(Url,String.class);
                log.info("通知管理员Cookies可能失效了" + entity.getBody());
                //以上可以删除
            }
        }

    }

    @Override
    public String getName() {
        return taskName;
    }
}
