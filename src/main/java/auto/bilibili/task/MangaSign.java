package auto.bilibili.task;

import auto.bilibili.apiquery.ApiList;
import auto.bilibili.config.Config;
import auto.bilibili.utils.HttpUtil;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;


/**
 * 漫画签到
 *
 * @author @JunzhouLiu @Kurenai
 * @since 2020-11-22 5:22
 */

@Slf4j
public class MangaSign implements Task {


    private final String taskName = "漫画签到";

    @Override
    public void run() {

        String platform = Config.devicePlatform;
        String requestBody = "platform=" + platform;
        JsonObject result = HttpUtil.doPost(ApiList.Manga, requestBody);

        if (result == null) {
            log.info("哔哩哔哩漫画已经签到过了");
        } else {
            log.info("完成漫画签到");
        }
    }

    @Override
    public String getName() {
        return taskName;
    }
}
