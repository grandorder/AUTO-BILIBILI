package auto.bilibili.bean;

import lombok.Data;

@Data
public class b站签到用户 {
    private int id;
    private String DEDEUSERID;
    private String SESSDATA;
    private String BILI_JCT;
    private int reserveCoins;
    private int upLive;
    private int chargeForLove;
    private String userAgent;
    private int skipDailyTask;
    private String log;

}
