package auto.bilibili.visit;

import auto.bilibili.bean.b站签到用户;
import auto.bilibili.config.Config;
import auto.bilibili.login.Verify;
import auto.bilibili.mapper.CommonMapper;
import auto.bilibili.task.DailyTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Component
public class StartRun implements ApplicationRunner {

    @Autowired
    CommonMapper commonMapper;

    //这个注解受spring容器控制，在里面执行退出命令不能终止项目
    @PostConstruct
    public  void  webRequest()  {
    }

    @Override
    public void run(ApplicationArguments args) throws InterruptedException {
        int sleepTime = (int) ((new Random().nextDouble() + 0.5) *600*5);
        System.out.println("开始执行每日签到,为避免开始时间固定，启动时进行--随机暂停"+sleepTime+"ms-----\n");
        Thread.sleep(sleepTime);
        //设置日期格式 ,new Date()为获取当前系统时间
        System.out.println("开始执行时间-----"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

        //获取所有用户
        List<b站签到用户> users= commonMapper.selectAll();
        //进行判读是否有效，把获取到的用户注入登录账号中
        for (b站签到用户 user : users) {
            if (user.getLog().contains("失败")||user.getSkipDailyTask()!=0){
                System.out.println("该用户上次执行失败,本次签到跳过或" +
                        "已开启了跳过本日任务，本日任务跳过，如果需要取消跳过，请将skipDailyTask值改为1");
            }else {
                commonMapper.updateTaskInfo(user.getDEDEUSERID(),"正常执行");
                this.BiliMain(user);
            }
        }

        //完成任务后结束springboot项目
        System.out.println("所有签到任务结束，退出springboot项目-----"
                +new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        System.exit(0);
    }

    private void BiliMain(b站签到用户 user){
        //读取环境变量
        Verify.verifyInit(user.getDEDEUSERID(), user.getSESSDATA(), user.getBILI_JCT());
        //配置签到信息
        Config.chargeForLove=String.valueOf(user.getChargeForLove()) ;
        Config.reserveCoins=user.getReserveCoins();
        Config.upLive=String.valueOf(user.getUpLive());
        //获取完整的浏览器 UA
        Config.userAgent=commonMapper.queryUserAgent(user.getUserAgent());
        DailyTask dailyTask = new DailyTask();
        dailyTask.doDailyTask();
    }


}
